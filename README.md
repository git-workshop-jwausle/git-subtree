# git-subtree

Upstream project to integrate it into multiple downstream projects as subtree. 

```
git remote add subtree-child git@gitlab.com:git-workshop-jwausle/git-subtree.git
git subtree add --prefix=child-dir subtree-child git@gitlab.com:git-workshop-jwausle/git-subtree.git main --squash
```

## Use subtree child

```
git subtree pull-all
git subtree push-all 
```

## Drawbacks

* It isn't readily apparent that part of the main repo is built from a subtree
* You can't easily list the subtrees in your project
* You can't, at least easily, list the remote repositories of the subtrees
* The logs are slightly confusing when you update the host repository with subtree commits, then push the subtree to its host, and then pull the subtree.

> [Source](https://gist.github.com/SKempin/b7857a6ff6bddb05717cc17a44091202#subtree-issues)
